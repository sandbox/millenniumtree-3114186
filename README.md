# About

Commerce Quality of Life aims to provide on-site guidance for store owners and developers.

Some examples of what will be done include:

* Linking between related config pages (such as a link to the related store from the product edit page, or guiding you through creating attributes and faceted searching)
* Detecting and reporting on potential problems
* Possibly suggesting modules to install to accomplish related tasks
