<?php

namespace Drupal\commerce_qol;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_product\ProductTypeListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines the list builder for product types.
 */
class QOLProductListBuilder extends ProductTypeListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();

    foreach($header as $key => $val) {
      if($key == 'operations') {
        $header_altered['order_item_type'] = $this->t('Order item type');
        $header_altered['atc_form'] = $this->t('Add-to-cart form');
      }
      $header_altered[$key] = $val;
    }

    return $header_altered;
  }

  /**
   * @param ProductType $entity
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $entity_type_manager = \Drupal::entityTypeManager();

    $variationTypeStorage = $entity_type_manager->getStorage('commerce_product_variation_type');
    /** @var ProductVariationType $variation_type */
    $variation_type = $variationTypeStorage->load($entity->getVariationTypeId());

    $orderItemTypeStorage = $entity_type_manager->getStorage('commerce_order_item_type');
    /** @var OrderItemType $order_item_type */
    $order_item_type = $orderItemTypeStorage->load($variation_type->getOrderItemTypeId());

    $row = parent::buildRow($entity);

    foreach($row as $key => $val) {
      if($key == 'operations') {

        $row_altered['order_item_type']['data'][] = [
          '#type' => 'link',
          '#title' => $order_item_type->label(),
          '#url' => $order_item_type->toUrl('edit-form'),
        ];

        $default_url = Url::fromUserInput($order_item_type->toUrl('edit-form')->toString().'/form-display');
        $atc_url = Url::fromUserInput($order_item_type->toUrl('edit-form')->toString().'/form-display/add_to_cart');
        $row_altered['atc_form']['data'][] = [
          '#type' => 'link',
          '#title' => $this->t('Add-to-cart form'),
          '#url' => $atc_url->access() ? $atc_url: $default_url,
        ];
      }
      $row_altered[$key] = $val;
    }

    return $row_altered;
  }

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   *
   * @todo Add a link to add a new item to the #empty text.
   */
  public function render() {
    $render = parent::render();

    $form = [
      'product_variation_type_link' => [
        '#type' => 'link',
        '#title' => $this->t('Product variation types'),
        '#url' => Url::fromRoute('entity.commerce_product_variation_type.collection'),
      ],
    ];

    return array_merge($form, $render);
  }

}
